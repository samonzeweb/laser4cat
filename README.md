# Laser 4 Cat
(_le chat machine_)

Ce programme destiné à tourner sur une plateforme Arduino sert à piloter un... jouet pour chat !

Le principe est simple : un laser de faible puissance est monté sur deux servomoteurs, et le programme génère des mouvements aléatoires. De temps en temps le laser peut s'éteindre, ou les servos générer une série de secousses.

Divers paramètres sont ajustables pour adapter l'usage en fonction de son espace ou le tempérament du chat :

* l'amplitude des servos (séparéments)
* l'amplitude de temporisation aléatoire entre les déplacements.
* la probabilité que le laser soit allumé ou non.
* la probabilité de produire des secousses entre les déplacements.
* l'amplitude, le nombre et la rapidité des secousses.

Les paramètres sont tous indiqués clairement dans le code.

# Technique

Par défaut les servos utilisent les broches 10 et 11 (PWM), et le laser la broche 12. Ces éléments sont clairement indiqués dans le code et peuvent être changés.

Le laser que j'ai utilisé est récupéré d'un jouet pour chat et consomme environ 10mA, il peut être alimenté directement par le microcontrôleur ce qui permet de l'éteindre à volonté. Mais c'est à vérifier au cas par cas, et dans le doute le mieux est de ne pas le connecter au microcontrôleur mais directement à une source l'alimentation continue.

Par sécurité le laset doit être au plus de classe 2 : https://fr.wikipedia.org/wiki/Laser
