#include <Servo.h>

// Brochage utilisé
int laserPin      = 12;
int profondeurPin = 11;
int lacetPin      = 10;

// Limites angulaire de course des servos
// position la plus à droite (vue du pointeur)
const int lacetMin =  45;
// position la plus à gauche
const int lacetMax = 135;
// position la plus haute
const int profondeurMin = 60;
// position la plus basse
const int profondeurMax = 120;

// Temps de pause (1/10ème de seconde)
const int pauseMin = 5;
const int pauseMax = 50;

// Allumage du laser, probabilité d'extinction  = 1 / probabiliteLaser)
const int probabiliteLaser = 20;

// Probabilité du "mode secousse" (1 / probiliteSecousses)
const int probiliteSecousses = 10;

// Amplitude des secousses (angulaire)
const int amplitudeSecousses = 10;
// Nombre de secousses;
const int nombreSecousses = 10;
// Durée entre chaque secousse (ms)
const int temporisationSecousses = 100;

// Objets de contrôle des servos
Servo profondeur;
Servo lacet;

// Initialisation
void setup() {
  // Ne pas avoir la même séquence à chaque fois
  randomSeed(analogRead(0));
  
  // Initialisation et centrage des servos
  profondeur.attach(profondeurPin);
  profondeur.write(90);
  lacet.attach(lacetPin);
  lacet.write(90);
  
  // Temps d'attente (permet de vérifier le centrage)
  delay(5000);
  
  // Allumage du laser.. Goldorak GO !
  pinMode(laserPin, OUTPUT);
  digitalWrite(laserPin, HIGH);
}

// Boucle principale
void loop() {
  int posProfondeur, posLacet;
  
  // Nouvelle position du pointeur
  DeplaceLaser(posProfondeur, posLacet);

  // Determiner si la laser est allumé ou éteint
  if(random(probabiliteLaser) == 0) {
    digitalWrite(laserPin, LOW);
  } else {
    digitalWrite(laserPin, HIGH);
  }
  
  // Secousses ?
  if(random(probiliteSecousses) == 0) {
    Secouer(posProfondeur, posLacet);
  }
  
  // Effectuer une pause alléatoire
  delay(100 * random(pauseMin, pauseMax + 1));
}

// Change la direction du pointeur
void DeplaceLaser(int &posProfondeur, int &posLacet) {
  // Calculer la nouvelle position des servos
  posProfondeur = random(profondeurMin, profondeurMax + 1);
  posLacet = random(lacetMin, lacetMax + 1);
  // Ordonner aux servos de pointer le laser au nouvel emplacement
  profondeur.write(posProfondeur);
  lacet.write(posLacet);  
}

// Produit la série de secousses
void Secouer(int posProfondeur, int posLacet) {
  int profondeurSecousse, lacetSecousse;
  
  for(int noSecousse = 0 ; noSecousse < nombreSecousses ; noSecousse++ ) {
    // Déplacer le laser
    profondeurSecousse = PositionSecousse(posProfondeur, profondeurMin, profondeurMax);
    profondeur.write(profondeurSecousse);
    lacetSecousse      = PositionSecousse(posLacet, lacetMin, lacetMax);
    lacet.write(lacetSecousse);
    // Temporisation
    delay(temporisationSecousses);
  }
}

// Calculer une nouvelle position (profondeur ou lacet) pour une secousse
int PositionSecousse(int posCourante, int posMin, int posMax) {
  int minVariation = - amplitudeSecousses / 2;
  int maxVariation = minVariation + amplitudeSecousses;
  int posSecousse = posCourante + random(amplitudeSecousses) - amplitudeSecousses/2;
  if(posSecousse < posMin) {
    posSecousse = posMin;
  } else if (posSecousse > posMax) {
    posSecousse = posMax;
  }
  return posSecousse;
}

